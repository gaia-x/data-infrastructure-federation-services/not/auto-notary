package eu.gaiax.notarization.auto_notary.application;

import eu.gaiax.notarization.auto_notary.application.model.ApprovalResult;
import eu.gaiax.notarization.auto_notary.application.model.ApprovalResults;
import eu.gaiax.notarization.auto_notary.application.model.ApprovalStatus;
import eu.gaiax.notarization.auto_notary.web.client.RequestProcessingRestClient;
import eu.gaiax.notarization.auto_notary.web.client.RequestProcessingRestClient.PagedNotarizationRequestSummary;
import eu.gaiax.notarization.auto_notary.web.client.RequestProcessingRestClient.PagedNotarizationRequestSummary.NotarizationRequestSummary;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class AutoApprovingService {

    public static final int PAGE_SIZE = 50;

    @Inject
    @RestClient
    RequestProcessingRestClient requestProcessingClient;

    @Inject
    Logger logger;

    public Uni<ApprovalResults> approveAvailableRequests() {
        final String availableStatus = "available";

        return claimAndAccept(availableStatus);
    }

    private Uni<ApprovalResults> claimAndAccept(final String availableStatus) {

        return pageAll(availableStatus)
                .onItem().transformToUniAndMerge(this::claimAndAccept).collect()
                .asList().map(this::count);
    }

    private ApprovalResults count(List<ApprovalResult> items) {

        var uniqueItems = Set.copyOf(items);
        long success = 0;
        long failure = 0;

        for (ApprovalResult item : uniqueItems) {
            switch (item.status()) {
                case Success -> success += 1;
                case Failure -> failure += 1;
                default -> throw new IllegalArgumentException("Unknown approval result");
            }
        }

        return new ApprovalResults(success, failure);
    }

    private Multi<NotarizationRequestSummary> pageAll(String status) {
        final AtomicLong previousStatus = new AtomicLong(0L);

        return Multi.createBy().repeating().uni(() -> {
            return requestProcessingClient.fetchRequests(0, PAGE_SIZE, status);
        }).whilst(page -> {
            final long requestCount = page.requestCount();
            return hasRequests(page) && requestCount != previousStatus.getAndSet(requestCount);
        }).onItem()
                .transformToMultiAndMerge(response -> Multi.createFrom().iterable(response.notarizationRequests()));
    }

    private boolean hasRequests(PagedNotarizationRequestSummary page) {
        return page != null && page.requestCount() > 0 && page.notarizationRequests() != null && !page.notarizationRequests().isEmpty();
    }

    public Uni<ApprovalResult> claimAndAccept(NotarizationRequestSummary request) {
        if (request == null) {
            return Uni.createFrom().item(new ApprovalResult(null, ApprovalStatus.Failure));
        }
        final String profileId = request.profileId();
        final String id = request.id();
        return claimAndAccept(profileId, id);
    }

    public Uni<ApprovalResult> claimAndAccept(final String profileId, final String id) {
        if (profileId == null || id == null) {
            return Uni.createFrom().item(new ApprovalResult(id, ApprovalStatus.Failure));
        }
        return requestProcessingClient.claim(profileId, id)
                .onItem().transformToUni((v) -> requestProcessingClient.accept(profileId, id))
                .replaceWith(new ApprovalResult(id, ApprovalStatus.Success))
                .onFailure().invoke(t -> logger.warnv("Could not claim request {0}!", id, t))
                .onFailure().recoverWithItem(new ApprovalResult(id, ApprovalStatus.Failure));
    }

    public Uni<ApprovalResults> approveOwnRequests() {
        final String availableStatus = "ownClaimed";

        return pageAll(availableStatus)
                .onItem().transformToUniAndMerge(this::claimAndAccept).collect()
                .asList().map(this::count);
    }

    public Uni<ApprovalResult> accept(NotarizationRequestSummary request) {
        if (request == null) {
            return Uni.createFrom().item(new ApprovalResult(null, ApprovalStatus.Failure));
        }
        final String profileId = request.profileId();
        final String id = request.id();
        return accept(profileId, id);
    }

    public Uni<ApprovalResult> accept(final String profileId, final String id) {
        if (profileId == null || id == null) {
            return Uni.createFrom().item(new ApprovalResult(id, ApprovalStatus.Failure));
        }
        return requestProcessingClient.accept(profileId, id)
                .replaceWith(new ApprovalResult(id, ApprovalStatus.Success))
                .onFailure().invoke(t -> logger.warnv("Could not accept request {0}!", id, t))
                .onFailure().recoverWithItem(new ApprovalResult(id, ApprovalStatus.Failure));
    }
}
