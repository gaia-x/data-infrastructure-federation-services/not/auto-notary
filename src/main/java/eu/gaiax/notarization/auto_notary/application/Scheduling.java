
package eu.gaiax.notarization.auto_notary.application;

import io.quarkus.scheduler.Scheduled;
import io.smallrye.mutiny.Uni;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 *
 * @author Neil Crossley
 */
@ApplicationScoped
public class Scheduling {

    @Inject
    AutoApprovingService autoApprovingService;

    @Scheduled(cron = "{cron.auto-accept}")
    public Uni<Void> approveOwnRequests() {

        return autoApprovingService.approveOwnRequests().replaceWithVoid();
    }

    @Scheduled(cron = "{cron.auto-approve-accept}")
    public Uni<Void> approveAvailableRequests() {
        return autoApprovingService.approveAvailableRequests().replaceWithVoid();
    }

}
