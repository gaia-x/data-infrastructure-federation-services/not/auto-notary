
package eu.gaiax.notarization.auto_notary.application.model;

/**
 *
 * @author Neil Crossley
 */
public record ApprovalResults(long success, long failure) {

}
