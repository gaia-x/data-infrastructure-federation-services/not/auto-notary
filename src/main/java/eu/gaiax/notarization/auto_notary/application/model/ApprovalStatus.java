
package eu.gaiax.notarization.auto_notary.application.model;

/**
 *
 * @author Neil Crossley
 */
public enum ApprovalStatus {

    Success,
    Failure
}
