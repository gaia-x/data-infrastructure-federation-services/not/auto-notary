package eu.gaiax.notarization.auto_notary.web.client;

import io.quarkus.oidc.client.reactive.filter.OidcClientRequestReactiveFilter;
import io.smallrye.mutiny.Uni;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.eclipse.microprofile.faulttolerance.Bulkhead;
import org.eclipse.microprofile.faulttolerance.CircuitBreaker;
import org.eclipse.microprofile.faulttolerance.Timeout;
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestQuery;

/**
 *
 * @author Neil Crossley
 */
@Path("/api/v1")
@RegisterRestClient(configKey = "requestprocessing-api")
@RegisterProvider(OidcClientRequestReactiveFilter.class)
public interface RequestProcessingRestClient {

    @GET
    @Path("/requests")
    @CircuitBreaker(requestVolumeThreshold = 4)
    @Timeout(value = 2500L, unit = ChronoUnit.MILLIS)
    Uni<PagedNotarizationRequestSummary> fetchRequests(
	    @RestQuery Integer offset,
	    @RestQuery Integer limit,
	    @RestQuery String filter);

    @POST
    @Path("/profiles/{profileId}/requests/{notarizationRequestId}/claim")
    @CircuitBreaker(requestVolumeThreshold = 10)
    @Timeout(value = 2000L, unit = ChronoUnit.MILLIS)
    @Bulkhead(value = 30, waitingTaskQueue = 50)
    Uni<Void> claim(@RestPath String profileId, @RestPath String notarizationRequestId);

    @POST
    @Path("/profiles/{profileId}/requests/{notarizationRequestId}/accept")
    @CircuitBreaker(requestVolumeThreshold = 5)
    @Timeout(value = 3000L, unit = ChronoUnit.MILLIS)
    @Bulkhead(value = 30, waitingTaskQueue = 50)
    Uni<Void> accept(@RestPath String profileId, @RestPath String notarizationRequestId);

    public record PagedNotarizationRequestSummary(
	    int pageCount,
	    long requestCount,
	    List<NotarizationRequestSummary> notarizationRequests) {

	public static record NotarizationRequestSummary(
		String id,
		String profileId,
		OffsetDateTime createdAt,
		OffsetDateTime lastModified,
		String requestState,
		String holder,
		int totalDocuments,
		String rejectComment) {

	}

    }

}
