package eu.gaiax.notarization.auto_notary.web.resources;

import eu.gaiax.notarization.auto_notary.application.AutoApprovingService;
import eu.gaiax.notarization.auto_notary.application.model.ApprovalResults;
import io.smallrye.mutiny.Uni;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/trigger")
public class TriggerResource {

    @Inject
    AutoApprovingService autoApprovingService;

    @POST
    @Path("/available")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<ApprovalResults> triggerAvailable() {

        return autoApprovingService.approveAvailableRequests();
    }

    @POST
    @Path("/ownClaimed")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<ApprovalResults> triggerOwn() {

        return autoApprovingService.approveOwnRequests();
    }
}
