package eu.gaiax.notarization.auto_notary.web.client;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import com.github.tomakehurst.wiremock.core.Options.ChunkedEncodingPolicy;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Neil Crossley
 */
public class MockAuthServerResource implements QuarkusTestResourceLifecycleManager {

    public static final String ACCESS_TOKEN_1 = UUID.randomUUID().toString();

    public static final String username = "user" + UUID.randomUUID().toString();
    public static final String password = "password" + UUID.randomUUID().toString();
    public static final String clientId = "clientId" + UUID.randomUUID().toString();

    private WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        wireMockServer = new WireMockServer(wireMockConfig()
                .dynamicPort()
                .useChunkedTransferEncoding(ChunkedEncodingPolicy.NEVER));
        wireMockServer.start();
        wireMockServer.stubFor(WireMock.post("/token")
                .withRequestBody(matching(String.format(
                        "grant_type=password&username=%s&password=%s&client_id=%s",
                        username,
                        password,
                        clientId
                )))
                .willReturn(WireMock
                        .aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(
                                "{\"access_token\":\"" + ACCESS_TOKEN_1 + "\", \"expires_in\":600 }"
                        )));

        return Map.of("quarkus.oidc-client.auth-server-url", wireMockServer.baseUrl(),
                "quarkus.oidc-client.grant-options.password.username", username,
                "quarkus.oidc-client.grant-options.password.password", password,
                "quarkus.oidc-client.client-id", clientId);
    }

    @Override
    public synchronized void stop() {
        if (wireMockServer != null) {
            wireMockServer.stop();
            wireMockServer = null;
        }
    }

    @Override
    public void inject(QuarkusTestResourceLifecycleManager.TestInjector testInjector) {
        testInjector.injectIntoFields(
                wireMockServer,
                new QuarkusTestResourceLifecycleManager.TestInjector.AnnotatedAndMatchesType(
                        MockAuthServer.class, WireMockServer.class));
    }
}
