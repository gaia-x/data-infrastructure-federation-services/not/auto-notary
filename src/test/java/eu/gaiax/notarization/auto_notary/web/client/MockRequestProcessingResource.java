package eu.gaiax.notarization.auto_notary.web.client;

import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import java.util.Map;

/**
 *
 * @author Neil Crossley
 */
public class MockRequestProcessingResource implements QuarkusTestResourceLifecycleManager {

    WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        wireMockServer = new WireMockServer(wireMockConfig().dynamicPort());
        wireMockServer.start();

        return Map.of("quarkus.rest-client.requestprocessing-api.url", wireMockServer.baseUrl());
    }

    @Override
    public synchronized void stop() {
        if (wireMockServer != null) {
            wireMockServer.stop();
            wireMockServer = null;
        }
    }

    @Override
    public void inject(QuarkusTestResourceLifecycleManager.TestInjector testInjector) {
        testInjector.injectIntoFields(
                wireMockServer,
                new QuarkusTestResourceLifecycleManager.TestInjector.AnnotatedAndMatchesType(
                        MockRequestProcessing.class, WireMockServer.class));
    }
}
