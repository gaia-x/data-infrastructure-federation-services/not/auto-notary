package eu.gaiax.notarization.auto_notary.web.resources;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import com.github.tomakehurst.wiremock.stubbing.Scenario;
import eu.gaiax.notarization.auto_notary.application.AutoApprovingService;
import eu.gaiax.notarization.auto_notary.web.client.MockAuthServerResource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ws.rs.core.MediaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import eu.gaiax.notarization.auto_notary.web.client.RequestProcessingRestClient.PagedNotarizationRequestSummary;
import eu.gaiax.notarization.auto_notary.web.client.RequestProcessingRestClient.PagedNotarizationRequestSummary.NotarizationRequestSummary;
import java.time.OffsetDateTime;

/**
 *
 * @author Neil Crossley
 */
public class RequestProcessingStub {

    private static record Request(String id, String profileId) {}

    private static final String stateClaimed = "claimed";
    private static final String stateAccepted = "accepted";
    private static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.registerModule(new JavaTimeModule());
    }

    private List<Request> availableRequests = new ArrayList<>();
    private List<Request> errorAvailableRequests = new ArrayList<>();
    private List<Request> claimedRequests = new ArrayList<>();
    private List<Request> errorClaimedRequests = new ArrayList<>();
    final WireMockServer wireMock;

    public RequestProcessingStub(WireMockServer wireMock) {
        this.wireMock = wireMock;
    }

    private void addClaimableAcceptableRequest(String id, String profileId) {
        final Request request = new Request(id, profileId);
        availableRequests.add(request);

        String scenario = requestScenario(id);
        String path = requestPath(request);

        wireMock.stubFor(post(urlPathEqualTo(path + "claim"))
                .inScenario(scenario)
                .whenScenarioStateIs(Scenario.STARTED)
                .withHeader("Authorization", WireMock.equalTo("Bearer " + MockAuthServerResource.ACCESS_TOKEN_1))
                .willReturn(aResponse()
                        .withStatus(201)
                ).atPriority(100)
                .willSetStateTo(stateClaimed)
        );
        wireMock.stubFor(post(urlPathEqualTo(path + "accept"))
                .inScenario(scenario)
                .whenScenarioStateIs(stateClaimed)
                .withHeader("Authorization", WireMock.equalTo("Bearer " + MockAuthServerResource.ACCESS_TOKEN_1))
                .willReturn(aResponse()
                        .withStatus(201)
                ).atPriority(100)
                .willSetStateTo("done")
        );
    }

    private String requestScenario(String id) {
        var scenario = "scenario " + id;
        return scenario;
    }

    private String requestPath(Request request) {
        var path = "/api/v1/profiles/" + request.profileId + "/requests/" + request.id + "/";
        return path;
    }

    public void addClaimableRequest() {
        this.addClaimableAcceptableRequest("id" + UUID.randomUUID(), "profile" + UUID.randomUUID());
    }

    void addFailingClaimableRequest() {

        this.addFailingClaimableRequest("id" + UUID.randomUUID(), "profile" + UUID.randomUUID(), 400);
    }

    private void addFailingClaimableRequest(String id, String profileId, int status) {
        final Request request = new Request(id, profileId);
        errorAvailableRequests.add(request);

        String path = requestPath(request);

        wireMock.stubFor(post(urlPathEqualTo(path + "claim"))
                .withHeader("Authorization", WireMock.equalTo("Bearer " + MockAuthServerResource.ACCESS_TOKEN_1))
                .willReturn(aResponse()
                        .withStatus(status)
                ).atPriority(100)
        );
    }
    public void registerRequests() {

        registerPagingRequests(availableRequests, errorAvailableRequests, "Paging, Claiming, Accepting", "available");
        registerPagingRequests(this.claimedRequests, errorClaimedRequests, "Paging, Accepting", "ownClaimed");
    }

    private void registerPagingRequests(final List<Request> requests, List<Request> errors, String requestsScenario, final String filterType) throws IllegalArgumentException {
        String firstPageProcessed = "firstPageProcessed" + filterType;

        wireMock.stubFor(get(urlPathEqualTo("/api/v1/requests"))
                .inScenario(requestsScenario)
                .whenScenarioStateIs(Scenario.STARTED)
                .withQueryParam("offset", WireMock.equalTo("0"))
                .withQueryParam("limit", WireMock.equalTo(Integer.toString(AutoApprovingService.PAGE_SIZE)))
                .withQueryParam("filter", WireMock.equalTo(filterType))
                .withHeader("Authorization", WireMock.equalTo("Bearer " + MockAuthServerResource.ACCESS_TOKEN_1))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                        .withJsonBody(asJsonSummary(requests, errors))
                ).atPriority(100)
                .willSetStateTo(firstPageProcessed));

        wireMock.stubFor(get(urlPathEqualTo("/api/v1/requests"))
                .inScenario(requestsScenario)
                .whenScenarioStateIs(firstPageProcessed)
                .withQueryParam("offset", WireMock.equalTo("0"))
                .withQueryParam("limit", WireMock.equalTo(Integer.toString(AutoApprovingService.PAGE_SIZE)))
                .withQueryParam("filter", WireMock.equalTo(filterType))
                .withHeader("Authorization", WireMock.equalTo("Bearer " + MockAuthServerResource.ACCESS_TOKEN_1))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                        .withJsonBody(asJsonSummary(errors))
                ).atPriority(100));
    }

    private JsonNode asJsonSummary(final List<Request>... allRequests) {
        return objectMapper.valueToTree(asSummary(allRequests));
    }

    private PagedNotarizationRequestSummary asSummary(final List<Request>... allRequests) {

        List<NotarizationRequestSummary> results = new ArrayList<>();
        for (List<Request> requests : allRequests) {
            for (Request request : requests) {
                results.add(new NotarizationRequestSummary(
                        request.id,
                        request.profileId,
                        OffsetDateTime.now().minusHours(2),
                        OffsetDateTime.now().minusHours(1),
                        "readyForReview",
                        "did:something:or-other",
                        0,
                        null
                ));
            }
        }

        PagedNotarizationRequestSummary summary = new PagedNotarizationRequestSummary(
                1, results.size(), results);
        return summary;
    }
}
