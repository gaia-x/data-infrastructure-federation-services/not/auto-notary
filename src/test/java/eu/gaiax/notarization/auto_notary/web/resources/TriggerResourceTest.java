package eu.gaiax.notarization.auto_notary.web.resources;

import com.github.tomakehurst.wiremock.WireMockServer;
import eu.gaiax.notarization.auto_notary.web.client.MockAuthServer;
import eu.gaiax.notarization.auto_notary.web.client.MockAuthServerResource;
import eu.gaiax.notarization.auto_notary.web.client.MockRequestProcessing;
import eu.gaiax.notarization.auto_notary.web.client.MockRequestProcessingResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;

import static io.restassured.RestAssured.given;
import javax.inject.Inject;
import static org.hamcrest.Matchers.equalTo;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@QuarkusTest
@QuarkusTestResource(MockRequestProcessingResource.class)
@QuarkusTestResource(MockAuthServerResource.class)
public class TriggerResourceTest {

    @MockRequestProcessing
    WireMockServer mockRequestProcessing;

    @MockAuthServer
    WireMockServer mockAuthServer;

    @Inject
    Logger logger;

    @BeforeEach
    public void setup() {
        mockRequestProcessing.resetAll();
    }

    @Test
    public void testSomeAvailableTrigger() {

        var setup = new RequestProcessingStub(mockRequestProcessing);
        setup.addClaimableRequest();
        setup.addClaimableRequest();
        setup.registerRequests();

        given()
                .when().post("/trigger/available")
                .then()
                .statusCode(200)
                .body("success", equalTo(2), "failure", equalTo(0));
    }

    @Test
    public void testSomeErrorTriggers() {

        var setup = new RequestProcessingStub(mockRequestProcessing);
        setup.addClaimableRequest();
        setup.addFailingClaimableRequest();
        setup.addClaimableRequest();
        setup.addClaimableRequest();
        setup.addFailingClaimableRequest();
        setup.registerRequests();

        given()
                .when().post("/trigger/available")
                .then()
                .statusCode(200)
                .body("success", equalTo(3), "failure", equalTo(2));
    }
}
